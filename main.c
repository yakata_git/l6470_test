/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F18346
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"

/*
                         Main application
 */
void L6470_sendSPI_Data(uint8_t *data,uint8_t datanum);
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    //INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    __delay_ms(500);
    reset_SetHigh();
    if(SPI1_Open(SPI1_DEFAULT))
    {
        uint8_t buf2[2]={0x00,0x00};
        buf2[0]=0x09;   buf2[1]=0x30; 
        L6470_sendSPI_Data(buf2,sizeof(buf2)); //KVAL_HOLD
        buf2[0]=0x0A;   buf2[1]=0xFF;
        L6470_sendSPI_Data(buf2,sizeof(buf2)); //KVAL_RUN
        
        uint8_t cmd_run[4]={0x51,0x11,0x11,0x11};
        L6470_sendSPI_Data(cmd_run,sizeof(cmd_run)); //_RUN
    }
    while (1)
    {
        LED_Toggle();
        __delay_ms(1000);

        //uint8_t data4[4]={0x51,0x00,0x00,0x10};
        //SPI1_WriteBlock(data4,sizeof(data4));
    }
}
void L6470_sendSPI_Data(uint8_t *data,uint8_t datanum)
{
    uint8_t bytesWritten = 0;

    if(datanum != 0)
    {
        while(bytesWritten < datanum)
        {
            CS_SetLow();
            SPI1_WriteByte(data[bytesWritten]);
            while(!SSP1STATbits.BF){}
            CS_SetHigh();  
            
            bytesWritten++;
        }
    }
}

    
/**
 End of File
*/