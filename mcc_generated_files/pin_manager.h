/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16F18346
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set RA2 procedures
#define RA2_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define RA2_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define RA2_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define RA2_GetValue()              PORTAbits.RA2
#define RA2_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define RA2_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define RA2_SetPullup()             do { WPUAbits.WPUA2 = 1; } while(0)
#define RA2_ResetPullup()           do { WPUAbits.WPUA2 = 0; } while(0)
#define RA2_SetAnalogMode()         do { ANSELAbits.ANSA2 = 1; } while(0)
#define RA2_SetDigitalMode()        do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set RB4 procedures
#define RB4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define RB4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define RB4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define RB4_GetValue()              PORTBbits.RB4
#define RB4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define RB4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define RB4_SetPullup()             do { WPUBbits.WPUB4 = 1; } while(0)
#define RB4_ResetPullup()           do { WPUBbits.WPUB4 = 0; } while(0)
#define RB4_SetAnalogMode()         do { ANSELBbits.ANSB4 = 1; } while(0)
#define RB4_SetDigitalMode()        do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set CS aliases
#define CS_TRIS                 TRISBbits.TRISB5
#define CS_LAT                  LATBbits.LATB5
#define CS_PORT                 PORTBbits.RB5
#define CS_WPU                  WPUBbits.WPUB5
#define CS_OD                   ODCONBbits.ODCB5
#define CS_ANS                  ANSELBbits.ANSB5
#define CS_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define CS_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define CS_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define CS_GetValue()           PORTBbits.RB5
#define CS_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define CS_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define CS_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define CS_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define CS_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define CS_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define CS_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define CS_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set LED aliases
#define LED_TRIS                 TRISBbits.TRISB7
#define LED_LAT                  LATBbits.LATB7
#define LED_PORT                 PORTBbits.RB7
#define LED_WPU                  WPUBbits.WPUB7
#define LED_OD                   ODCONBbits.ODCB7
#define LED_ANS                  ANSELBbits.ANSB7
#define LED_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define LED_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define LED_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define LED_GetValue()           PORTBbits.RB7
#define LED_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define LED_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define LED_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define LED_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define LED_SetPushPull()        do { ODCONBbits.ODCB7 = 0; } while(0)
#define LED_SetOpenDrain()       do { ODCONBbits.ODCB7 = 1; } while(0)
#define LED_SetAnalogMode()      do { ANSELBbits.ANSB7 = 1; } while(0)
#define LED_SetDigitalMode()     do { ANSELBbits.ANSB7 = 0; } while(0)

// get/set RC2 procedures
#define RC2_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define RC2_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define RC2_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define RC2_GetValue()              PORTCbits.RC2
#define RC2_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define RC2_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define RC2_SetPullup()             do { WPUCbits.WPUC2 = 1; } while(0)
#define RC2_ResetPullup()           do { WPUCbits.WPUC2 = 0; } while(0)
#define RC2_SetAnalogMode()         do { ANSELCbits.ANSC2 = 1; } while(0)
#define RC2_SetDigitalMode()        do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set reset aliases
#define reset_TRIS                 TRISCbits.TRISC7
#define reset_LAT                  LATCbits.LATC7
#define reset_PORT                 PORTCbits.RC7
#define reset_WPU                  WPUCbits.WPUC7
#define reset_OD                   ODCONCbits.ODCC7
#define reset_ANS                  ANSELCbits.ANSC7
#define reset_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define reset_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define reset_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define reset_GetValue()           PORTCbits.RC7
#define reset_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define reset_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define reset_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define reset_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define reset_SetPushPull()        do { ODCONCbits.ODCC7 = 0; } while(0)
#define reset_SetOpenDrain()       do { ODCONCbits.ODCC7 = 1; } while(0)
#define reset_SetAnalogMode()      do { ANSELCbits.ANSC7 = 1; } while(0)
#define reset_SetDigitalMode()     do { ANSELCbits.ANSC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/